<?php
    class Fruit {
        private $id;
        private $provenance;
        private $prix;
        private $quantity;
        private $name;


        public function __construct($id, $provenance, $prix, $quantity, $picture, $name){
            $this->id = $id;
            $this->provenance = $provenance;
            $this->prix = $prix;
            $this->quantity = $quantity;
            $this->picture = $picture;
            $this->name = $name;
        }


        public function getPrix()
        {
            return $this->prix;
        }

        public function setPrix($prix)
        {
            $this->prix = $prix;
        }

        public function getQuantity()
        {
            return $this->quantity;
        }


        public function setQuantity($quantity)
        {
            $this->quantity = $quantity;
        }


        public function getPicture()
        {
            return $this->picture;
        }

        public function setPicture($picture)
        {
            $this->picture = $picture;
        }
        private $picture;


        public function getId(){
            return $this->id;
        }

        public function setId($id){
            $this->id = $id;
        }

        public function getProvenance(){
            return $this->provenance;
        }

        public function setProvenance($provenance){
            $this->provenance = $provenance;
        }


        public function getName()
        {
            return $this->name;
        }

        public function setName($name)
        {
            $this->name = $name;
        }



    }
?>