<?php
abstract class DbManager {
    protected $bdd;

    private $host = 'database';
    private $user = 'root';
    private $password = 'tiger';
    private $dbName = 'demo-mvc';

    public function __construct(){
        try {
            $this->bdd = new PDO('mysql:host='.$this->host.';
            dbname='.$this->dbName.';charset=utf8',
                $this->user,
                $this->password);
        } catch (PDOException $e){
            var_dump($e);
            die();
        }
    }
}

/*
 *
 * $host = 'database';
    $dbName = 'examGaet1';
    $user = 'root';
    $password = 'tiger';
    $bdd = new PDO(
        'mysql:host='.$host.';dbname='.$dbName.';charset=utf8',
        $user,
        $password);
    $bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 */