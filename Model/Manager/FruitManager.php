<?php
    class FruitManager extends DbManager {

        public function getAll(){
            $query = $this->bdd->prepare("SELECT * FROM fruit");
            $query->execute();

            $results = $query->fetchAll();

            $fruits = [];
            foreach ($results as $res){
                $fruits[] = new Fruit($res['id'], $res['provenance'], $res['prix'],
                $res['quantity'], $res['picture'], $res['name']);
            }

            return $fruits;
        }

        public function insert(Fruit $fruit){
            $provenance = $fruit->getProvenance();
            $prix = $fruit->getPrix();
            $quantity = $fruit->getQuantity();
            $picture = $fruit->getPicture();
            $name = $fruit->getName();

            $query = $this->bdd->prepare(
                'INSERT INTO fruit ( provenance, prix, quantity, picture, name) VALUES ( :provenance, :prix, :quantity, :picture, :name)'
            );

            $query->bindParam(':provenance', $provenance);
            $query->bindParam(':prix', $prix);
            $query->bindParam(':quantity', $quantity);
            $query->bindParam(':picture', $picture);
            $query->bindParam(":name", $name);

            $query->execute();

            $fruit->setId($this->bdd->lastInsertId());

            return $fruit;

        }
    }
?>